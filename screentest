#!/usr/bin/python3

"""Display test patterns on monitors, beamers, outputs, etc. This can
be used to find dead pixels, check colors, focus, and so on. By
default, this will loop on a hardcoded list of patterns extracted from
the gst documentation, but patterns can be supplied by hand as
well. Also, it will use the resolution from the first monitor it finds
in the `wlr-rand` output, which might behave poorly in Xorg or
multi-monitor setups, use `--output` for the latter and `--resolution`
for the former.
"""

# This project has started after I failed to find a proper tool to do
# this when setting up a monitor at home. The colors *seemed* off, and
# focus seemed wrong. I remember a project called "screentest" that
# does this:
#
# https://github.com/TobiX/screentest
#
# ... but that tool was removed from Debian some time before Debian 12
# bookworm (2023) was released:
#
# https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1035806
#
# Alternatives include:
#
# http://www.brouhaha.com/~eric/software/lcdtest/
# https://github.com/aurelitec/injuredpixels-windows
# https://www.eizo.be/monitor-test/
# http://www.lagom.nl/lcd-test/
#
# the result is pretty good. i feel like it's lacking a bit on more
# specific cases like sharpness, gamma and so on, but for now, it will
# do.
#
# gst-launch is used. On a debian system that lives over gstreamer1.0-tools
# package.

import argparse
import logging
import subprocess
from typing import Optional, Iterator


class ThisArgumentParser(argparse.ArgumentParser):
    def __init__(self, *args, **kwargs):
        super().__init__(
            *args, description="Test screens with patterns.", epilog=__doc__, **kwargs
        )
        self.add_argument(
            "--output", help="guess resolution from that output, default: find first"
        )
        self.add_argument(
            "--resolution", help="use given resolution instead of guessing"
        )
        self.add_argument(
            "--patterns",
            help="list of patterns instead of default: "
            + ", ".join(list_gst_patterns()),
        )
        self.add_argument(
            "--overlay-name",
            action="store_true",
            help="show pattern name in an overlay",
        )
        self.add_argument(
            "--sink",
            help="output sink, default: %(default)s",
            default=["autovideosink"],
            # normally we should be able to do full screen with:
            # "waylandsink", "fullscreen=true",
            # but that crashes with:
            # ERROR: from element /GstPipeline:pipeline0/GstWaylandSink:waylandsink0: Window has no size set
            # that is a bug in sway, it works in weston:
            # https://github.com/swaywm/sway/issues/2176
            metavar="arg",
            nargs="+",
        )
        self.add_argument(
            "--audio",
            action="store_true",
            help="output audio to test output",
        )
        self.add_argument(
            "--audiofile",
            help="play given file, once, default to a 440Hz sine drone",
        )
        self.add_argument(
            "-q",
            "--quiet",
            action=LoggingAction,
            const="WARNING",
            help="silence messages except warnings and errors",
        )
        self.add_argument(
            "-d",
            "--debug",
            action=LoggingAction,
            const="DEBUG",
            help="enable debugging messages",
        )


class LoggingAction(argparse.Action):
    """change log level on the fly

    The logging system should be initialized before this, using
    `basicConfig`."""

    def __init__(self, *args, **kwargs):  # type: ignore[no-untyped-def]
        """setup the action parameters

        This enforces a selection of logging levels. It also checks if
        const is provided, in which case we assume it's an argument
        like `--verbose` or `--debug` without an argument.
        """
        kwargs["choices"] = logging._nameToLevel.keys()
        if "const" in kwargs:
            kwargs["nargs"] = 0
        super().__init__(*args, **kwargs)

    def __call__(self, parser, ns, values, option):  # type: ignore[no-untyped-def]
        """if const was specified it means argument-less parameters"""
        if self.const:
            logging.getLogger("").setLevel(self.const)
        else:
            logging.getLogger("").setLevel(values)
        # cargo-culted from _StoreConstAction
        setattr(ns, self.dest, self.const or values)


SAMPLE_WLR_OUTPUT = """eDP-1 "BOE 0x095F (eDP-1)"
  Physical size: 280x190 mm
  Enabled: yes
  Modes:
    2256x1504 px, 47.998001 Hz
    2256x1504 px, 59.999001 Hz (preferred, current)
  Position: 0,640
  Transform: normal
  Scale: 1.500000
DP-5 "Dell Inc. DELL U2723QE SERIAL (DP-5)"
  Physical size: 600x340 mm
  Enabled: yes
  Modes:
    720x400 px, 70.082001 Hz
    640x480 px, 59.939999 Hz
    640x480 px, 59.939999 Hz
    640x480 px, 60.000000 Hz
    640x480 px, 75.000000 Hz
    720x480 px, 59.939999 Hz
    720x480 px, 59.939999 Hz
    720x480 px, 60.000000 Hz
    720x480 px, 60.000000 Hz
    720x576 px, 50.000000 Hz
    720x576 px, 50.000000 Hz
    800x600 px, 60.317001 Hz
    800x600 px, 75.000000 Hz
    1024x768 px, 60.004002 Hz
    1024x768 px, 75.028999 Hz
    1280x720 px, 50.000000 Hz
    1280x720 px, 59.939999 Hz
    1280x720 px, 60.000000 Hz
    1152x864 px, 75.000000 Hz
    1280x800 px, 59.810001 Hz
    1280x1024 px, 60.020000 Hz
    1280x1024 px, 75.025002 Hz
    1680x1050 px, 59.953999 Hz
    1600x1200 px, 60.000000 Hz
    1920x1080 px, 23.976000 Hz
    1920x1080 px, 24.000000 Hz
    1920x1080 px, 50.000000 Hz
    1920x1080 px, 59.939999 Hz
    1920x1080 px, 60.000000 Hz
    1920x1080 px, 60.000000 Hz
    2048x1080 px, 23.997000 Hz
    1920x1200 px, 59.884998 Hz
    2048x1152 px, 60.000000 Hz
    2048x1280 px, 60.195999 Hz
    2560x1440 px, 59.951000 Hz
    3840x2160 px, 29.981001 Hz
    3840x2160 px, 59.997002 Hz (preferred, current)
  Position: 1504,640
  Transform: normal
  Scale: 1.500000
DP-7 "Dell Inc. DELL U2723QE SERIAL (DP-7)"
  Physical size: 600x340 mm
  Enabled: yes
  Modes:
    720x400 px, 70.082001 Hz
    640x480 px, 59.939999 Hz
    640x480 px, 59.939999 Hz
    640x480 px, 60.000000 Hz
    640x480 px, 75.000000 Hz
    720x480 px, 59.939999 Hz
    720x480 px, 59.939999 Hz
    720x480 px, 60.000000 Hz
    720x480 px, 60.000000 Hz
    720x576 px, 50.000000 Hz
    720x576 px, 50.000000 Hz
    800x600 px, 60.317001 Hz
    800x600 px, 75.000000 Hz
    1024x768 px, 60.004002 Hz
    1024x768 px, 75.028999 Hz
    1280x720 px, 50.000000 Hz
    1280x720 px, 59.939999 Hz
    1280x720 px, 60.000000 Hz
    1152x864 px, 75.000000 Hz
    1280x800 px, 59.810001 Hz
    1280x1024 px, 60.020000 Hz
    1280x1024 px, 75.025002 Hz
    1680x1050 px, 59.953999 Hz
    1600x1200 px, 60.000000 Hz
    1920x1080 px, 23.976000 Hz
    1920x1080 px, 24.000000 Hz
    1920x1080 px, 50.000000 Hz
    1920x1080 px, 59.939999 Hz
    1920x1080 px, 60.000000 Hz
    1920x1080 px, 60.000000 Hz
    2048x1080 px, 23.997000 Hz
    1920x1200 px, 59.884998 Hz
    2048x1152 px, 60.000000 Hz
    2048x1280 px, 60.195999 Hz
    2560x1440 px, 59.951000 Hz
    3840x2160 px, 29.981001 Hz
    3840x2160 px, 59.997002 Hz (preferred, current)
  Position: 4064,0
  Transform: 90
  Scale: 1.500000"""


def test_parse_wlr_randr_output():
    assert list(parse_wlr_randr_output(SAMPLE_WLR_OUTPUT)) == [
        ('eDP-1 "BOE 0x095F (eDP-1)"', False, 2256, 1504),
        ('DP-5 "Dell Inc. DELL U2723QE SERIAL (DP-5)"', False, 3840, 2160),
        ('DP-7 "Dell Inc. DELL U2723QE SERIAL (DP-7)"', True, 3840, 2160),
    ]


def parse_wlr_randr_output(out: str) -> Iterator[tuple[str, bool, int, int]]:
    """parse the wlr-randr output for monitors

    This returns a generator of tuples describing each monitor."""

    rotated = False  # if the current monitor is rotated
    output_line = ""  # the current monitor parsed
    x, y = None, None  # the current resolution extracted

    for line in out.split("\n"):
        if not line:  # skip empty lines
            continue
        # monitor outputs start with non-whitespace
        if not line.startswith(" "):
            if output_line and x and y:
                # we already have an existing monitor parsed, dump the stats
                logging.debug(
                    "output %s rotated: %s, %sx%s", output_line, rotated, x, y
                )
                yield (output_line, rotated, x, y)
            if output_line and not (x and y):
                logging.warning(
                    "found monitor without current resolution? ignoring %s", output_line
                )
            # new output found
            logging.debug("found output %s", line)
            output_line = line
            continue

        # check for rotation
        if "Transform: " in line:
            # 180 or 0 doesn't matter, but 90 or 270 do, and are treated the same
            rotated = "Transform: 90" in line or "Transform: 270" in line
            if rotated:
                logging.debug("rotated output: %s", output_line)
            continue

        # this is a crude heuristic, maybe a regex would be better...
        if "px," in line and "current" in line:
            # current output resolution: 3840x2160 px, 59.997002 Hz (preferred, current)
            x, y = tuple(map(int, line.split()[0].split("x", maxsplit=1)))
            logging.debug(
                "found resolution %sx%s for output %s, rotated: %s",
                x,
                y,
                output_line,
                rotated,
            )

    if not (output_line and x and y):
        logging.warning(
            "found incomplete output definition: %s, %s, %s", output_line, x, y
        )
        return
    logging.info("returning output: %s rotated: %s, %sx%s", output_line, rotated, x, y)
    yield (output_line, rotated, x, y)


def guess_resolution_wlr(output: Optional[str] = None) -> tuple[int, int]:
    # was originally this pipeline
    # resolution=$(wlr-randr  | grep current | head -1 | grep -P -o '[0-9]+x[0-9]+' | sed 's/\([0-9]*\)x\([0-9]*\)/width=\1,height=\2/')  # noqa: E501
    # output: width=2256,height=1504
    try:
        out = subprocess.check_output("wlr-randr", encoding="utf-8")
    except (subprocess.CalledProcessError, OSError) as e:
        logging.warning("failed to call wlr-randr: %s", e)
        out = ""
    for output_line, rotated, x, y in list(parse_wlr_randr_output(out)):
        # return first found or matching output
        if not output or output in output_line:
            if rotated:
                return y, x
            else:
                return x, y
    return 0, 0


def guess_resolution_screeninfo(output: Optional[str] = None) -> tuple[int, int]:
    try:
        from screeninfo import get_monitors
    except ImportError:
        logging.warning("cannot find screeninfo library")
        return 0, 0

    for m in get_monitors():
        if output is None or output in m.name:
            return m.width, m.height
    return 0, 0


def guess_resolution(output: Optional[str] = None) -> tuple[int, int]:
    x, y = guess_resolution_wlr(output)
    if x and y:
        return x, y

    logging.warning("output resolution not found with wlr-randr, trying screeninfo")
    x, y = guess_resolution_screeninfo(output)
    if x and y:
        return x, y

    logging.warning("output resolution not found, hardcoding to HD (1280x720)")
    return 1280, 720


def list_gst_patterns() -> Iterator[str]:
    """list of patterns copy-pasted from:
    https://gstreamer.freedesktop.org/documentation/videotestsrc/index.html?gi-language=c#GstVideoTestSrcPattern

    smpte (0) – SMPTE 100%% color bars
    snow (1) – Random (television snow)
    black (2) – 100%% Black
    white (3) – 100%% White
    red (4) – Red
    green (5) – Green
    blue (6) – Blue
    checkers-1 (7) – Checkers 1px
    checkers-2 (8) – Checkers 2px
    checkers-4 (9) – Checkers 4px
    checkers-8 (10) – Checkers 8px
    circular (11) – Circular
    blink (12) – Blink
    smpte75 (13) – SMPTE 75%% color bars
    zone-plate (14) – Zone plate
    gamut (15) – Gamut checkers
    chroma-zone-plate (16) – Chroma zone plate
    solid-color (17) – Solid color
    ball (18) – Moving ball
    smpte100 (19) – SMPTE 100%% color bars
    bar (20) – Bar
    pinwheel (21) – Pinwheel
    spokes (22) – Spokes
    gradient (23) – Gradient
    colors (24) – Colors
    smpte-rp-219 (25) – SMPTE test pattern, RP 219 conformant

    Could also be extracted from the output of `gst-inspect-1.0 videotestsrc`.
    """
    logging.debug("patterns: %s", list_gst_patterns.__doc__)
    for line in list_gst_patterns.__doc__.split("\n"):
        if " – " not in line:
            logging.debug("skipping %s", line)
            continue
        pattern = line.split()[0]
        if pattern == "blink":
            logging.info(
                "skipping %s pattern as it may cause seizures, use --pattern %s to enable explicitly",
                pattern,
                pattern,
            )
        yield pattern


def main():
    logging.basicConfig(format="%(message)s", level="INFO")
    parser = ThisArgumentParser()
    args = parser.parse_args()

    if args.patterns:
        patterns = args.patterns.split(",")
    else:
        patterns = list_gst_patterns()

    if args.resolution:
        x, y = map(int, args.resolution.split("x"))
        resolution = "width=%d,height=%s" % (x, y)
    else:
        resolution = "width=%d,height=%d" % guess_resolution(args.output)

    for pattern in patterns:
        cmd = [
            "gst-launch-1.0",
            "videotestsrc",
            "pattern=" + pattern,
            "!",
            "video/x-raw," + resolution,
        ]
        if args.overlay_name:
            cmd += [
                "!",
                "textoverlay",
                "text=%s" % pattern,
            ]
        cmd += ["!"] + args.sink
        if args.audio:
            if args.audiofile:
                cmd += [
                    ":",
                    "filesrc",
                    "location=%s" % args.audiofile,
                    "!",
                    "decodebin",
                ]
            else:
                cmd += [
                    ":",
                    "audiotestsrc",
                    "volume=0.4",
                ]
            cmd += [
                "!",
                "audioconvert",
                "!",
                "autoaudiosink",
            ]
        logging.info("launching pipeline with %s", cmd)
        subprocess.run(cmd)


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        pass
