#!/usr/bin/perl -w

# various slug implementations in Perl

use utf8;
use Unicode::Normalize;
use Text::Unidecode;
use URI::Escape q{uri_escape_utf8};

sub text_to_anchor_simple {
        my $str = shift;
        $str =~ s/^\s+//;
        $str =~ s/\s+$//;
        $str =~ s/\s/_/g;
        $str =~ s/"//g;
        $str =~ s/^[^a-zA-Z]/z-/; # must start with an alphabetical character
        $str = uri_escape_utf8($str);
        $str =~ s/%/./g;
        return $str;
}

# turn given string into a "slug", retaining whitespace and original unicode script
sub slugify {
        my $input = shift;

        $input = unidecode($input);    # turn unicode into plain latin script
        $input =~ tr/\000-\177//cd;    # Strip non-ASCII characters (>127)
        $input =~ s/[^\w\s-]//g;       # Remove all characters that are not word characters (includes _), spaces, or hyphens
        $input =~ s/^\s+|\s+$//g;      # Trim whitespace from both ends
        $input = lc($input);
        $input =~ s/[-\s]+/-/g;        # Replace all occurrences of spaces and hyphens with a single hyphen

        return $input;
}

sub slugify2 {
    my ($input) = @_;

    $input = NFKD($input);         # Normalize the Unicode string
    $input =~ tr/\000-\177//cd;    # Strip non-ASCII characters (>127)
    $input =~ s/[^\w\s-]//g;       # Remove all characters that are not word characters (includes _), spaces, or hyphens
    $input =~ s/^\s+|\s+$//g;      # Trim whitespace from both ends
    $input = lc($input);
    $input =~ s/[-\s]+/-/g;        # Replace all occurrences of spaces and hyphens with a single hyphen

    return $input;
}

my @strings = ("hello world", "北亰", "liberté");
for my $str (@strings) {
    print "string: $str\n";
    print "unidecode: " . slugify_nfkd("$str") . "\n";
    print "nfkd: ". slugify_unidecode("$str") . "\n";
}
