#!/bin/sh

# TODO: this should be made part of stressant, see
# https://gitlab.com/anarcat/stressant/-/issues/5

set -e

common_flags="--output-format=normal,terse,json+ --group_reporting --fallocate=none --ioengine=posixaio --runtime=60 --time_based --end_fsync=1"

while read type bs size jobs extra ; do
    name="${type}${bs}${size}${jobs}x$extra"
    echo "========== running job $name ==========" >&2
    fio $common_flags --name="$name" \
        --rw="$type" \
        --bs="$bs" \
        --size="$size" \
        --numjobs="$jobs" \
        --iodepth="$jobs" \
        $extra
done <<EOF
randread  4k 4g 1
randwrite 4k 4g 1
randread  64k 256m 16
randwrite 64k 256m 16
randread  1m 16g 1
randwrite 1m 16g 1
randread  4k 4g 1 --fsync=1
randwrite 4k 4g 1 --fsync=1
randread  64k 256m 16 --fsync=1
randwrite 64k 256m 16 --fsync=1
randread  1m 16g 1 --fsync=1
randwrite 1m 16g 1 --fsync=1
EOF

# to parse the results:
#
#    printf "test             \t\tread I/O\tread IOPS\twrite I/O\twrite IOPS\n"
#    grep '^3;' job.log | sort | cut -d';' -f3,7,8,48,49 | sed 's/;/\t/g'
#
# Or, as a markdown table:
#
#    printf "| test | read I/O | read IOPS | write I/O | write IOPS |\n"
#    grep '^3;' job.log | sort | cut -d';' -f3,7,8,48,49 | sed 's/;/ | /g;s/^/| /;s/$/ |/'
