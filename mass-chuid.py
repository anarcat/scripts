#!/usr/bin/python3
# coding: utf-8

'''review filesystem permissions and increment those matchin a minimum'''

# Copyright (C) 2016 Antoine Beaupré <anarcat@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import division, absolute_import
from __future__ import print_function, unicode_literals

import argparse
import logging
import os
import os.path
import posix1e
import stat
import sys
import tempfile


def parse_args(args=sys.argv[1:]):
    parser = argparse.ArgumentParser(description=__doc__,
                                     epilog='''''')
    loglevel = parser.add_mutually_exclusive_group()
    loglevel.add_argument('--verbose', '-v', dest='log_level',
                          action='store_const', const='info', default='warning')
    loglevel.add_argument('--debug', '-d', dest='log_level',
                          action='store_const', const='debug', default='warning')
    parser.add_argument('--selftest', '-s', action='store_true',
                        help='run self test suite and nothing else')
    parser.add_argument('--dryrun', '-n', action='store_true',
                        help='do nothing')
    parser.add_argument('--target', '-t', action='append', required=True,
                        help='target path')
    parser.add_argument('--minimum_uid', type=int, default=9000,
                        help='minimum UID to check (default: OVER NINE THOUSAND!!11)')
    parser.add_argument('--diff_uid', type=int, default=0,
                        help='how much to add to each UID (default: %(default)s)')
    parser.add_argument('--minimum_gid', type=int, default=9000,
                        help='minimum GID to check (default: OVER NINE THOUSAND!!11)')
    parser.add_argument('--diff_gid', type=int, default=0,
                        help='how much to add to each GID (default: %(default)s)')
    parser.add_argument('--acl', '-a', action='store_true',
                        help='additionally examine and modify extended ACLs')
    parser.add_argument('--recurse', '-r', action='store_true',
                        help='recursively process files and directories')

    args = parser.parse_args(args=args)

    if not (args.diff_uid or args.diff_gid):
        parser.error('Nothing to do, add --diff_uid or --diff_gid')

    return args


def main(args):
    for t in args.target:
        logging.info('# starting check of %s', t)
        process_path(t, args)
        if args.recurse:
            for root, dirs, files in os.walk(t, onerror=walk_handler):
                for name in files + dirs:
                    process_path(os.path.join(root, name), args)
        else:
            for entry in os.scandir(t):
                process_path(os.path.join(t, entry.name), args)


def walk_handler(exc):
    logging.warning('# scandir failed on %s: %s', exc.filename, exc)


def process_path(path, args):
    logging.debug('# process %s', path)
    try:
        st = os.stat(path)
    except OSError as e:
        logging.warning('# stat failed on %s: %s', e.filename, e)
        return

    if args.diff_uid and st.st_uid >= args.minimum_uid:
        new_uid = st.st_uid + args.diff_uid
    else:
        new_uid = -1
    if args.diff_gid and st.st_gid >= args.minimum_gid:
        new_gid = st.st_gid + args.diff_gid
    else:
        new_gid = -1

    if new_uid > -1 or new_gid > -1:
        if new_uid > -1 and new_gid > -1:
            logging.info('chown %d:%d %s', new_uid, new_gid, path)
        elif new_uid > -1 and new_gid == -1:
            logging.info('chown %d %s', new_uid, path)
        else:
            logging.info('chgrp %d %s', new_gid, path)

        if not args.dryrun:
            try:
                os.chown(path, new_uid, new_gid)
            except OSError as e:
                logging.warning('# chown failed on %s: %s', e.filename, e)
                pass

    has_extended = False
    apply_acl = False
    apply_dacl = False

    if args.acl:
        try:
            has_extended = posix1e.has_extended(path)
        except OSError:
            pass

    if has_extended:
        acl, macl, xacl, apply_acl = process_acl(posix1e.ACL(file=path), args)
        if apply_acl:
            macl_str = macl.to_any_text('', b',',
                                        (posix1e.TEXT_ABBREVIATE | posix1e.TEXT_NUMERIC_IDS))
            xacl_str = xacl.to_any_text('', b',',
                                        (posix1e.TEXT_ABBREVIATE | posix1e.TEXT_NUMERIC_IDS))
            logging.info('setfacl -m %s -x %s %s',
                         macl_str.decode('utf-8'), xacl_str.decode('utf-8'), path)

        if stat.S_ISDIR(st.st_mode):
            dacl, dmacl, dxacl, apply_dacl = process_acl(posix1e.ACL(filedef=path), args)
            if apply_dacl:
                dmacl_str = dmacl.to_any_text('', b',',
                                              (posix1e.TEXT_ABBREVIATE | posix1e.TEXT_NUMERIC_IDS))
                dxacl_str = dxacl.to_any_text('', b',',
                                              (posix1e.TEXT_ABBREVIATE | posix1e.TEXT_NUMERIC_IDS))
                logging.info('setfacl -d -m %s -x %s %s',
                             dmacl_str.decode('utf-8'), dxacl_str.decode('utf-8'), path)

        if not args.dryrun:
            if apply_acl:
                acl.applyto(path)
            if apply_dacl:
                dacl.applyto(path, posix1e.ACL_TYPE_DEFAULT)


def process_acl(acl, args):
    new_acl = posix1e.ACL()
    macl = posix1e.ACL()
    xacl = posix1e.ACL()
    modified_acl = False
    for entry in acl:
        new_entry = posix1e.Entry(new_acl)
        new_entry.copy(entry)
        if entry.tag_type == posix1e.ACL_USER and entry.qualifier > args.minimum_uid:
            new_entry.qualifier = entry.qualifier + args.diff_uid
            posix1e.Entry(xacl).copy(entry)
            posix1e.Entry(macl).copy(new_entry)
            modified_acl = True
        elif entry.tag_type == posix1e.ACL_GROUP and entry.qualifier > args.minimum_gid:
            new_entry.qualifier = entry.qualifier + args.diff_gid
            posix1e.Entry(xacl).copy(entry)
            posix1e.Entry(macl).copy(new_entry)
            modified_acl = True
    return new_acl, macl, xacl, modified_acl


def test_main():
    with tempfile.TemporaryDirectory() as tmpdir:
            args = parse_args(['--target', tmpdir])
            testfile = tmpdir + '/testfile'
            with open(testfile, 'w'):
                pass
            testdir = tmpdir + '/testdir'
            os.mkdir(testdir)
            assert os.path.exists(testfile)
            # this will probably fail without root
            os.chown(testfile, args.minimum_uid, args.minimum_uid)
            os.chown(testdir, args.minimum_uid, args.minimum_uid)
            stat = os.stat(testfile)
            old_uid = stat.st_uid
            assert old_uid == args.minimum_uid
            main(args)
            stat = os.stat(testfile)
            assert stat.st_uid == old_uid + args.diff_uid
            stat = os.stat(testdir)
            assert stat.st_uid == old_uid + args.diff_uid


if __name__ == '__main__':
    args = parse_args()
    logging.basicConfig(format='%(message)s', level=args.log_level.upper())
    if args.selftest:
        logging.info('# running test suite')
        test_main()
        sys.exit(0)
    main(args)
