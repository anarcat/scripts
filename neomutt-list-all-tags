#!/bin/sh

# this provides a way to replicate the "all tags" widget from
# notmuch-emacs in mutt. the way this works is that it shows first a
# list of "saved searches" then the "all tags" that have unread
# messages.
#
# this then gets loaded in notmuch with:
#
# unvirtual-mailboxes *
# virtual-mailboxes `/home/anarcat/bin/neomutt-list-all-tags`
#
# and that configuration can get reloaded with:
#
# unbind index,pager G
# macro index,pager G ":source ~/.neomuttrc\n"
#
# the `unvirtual-mailboxes` is necessary to purge messages that did
# get their unread messages cleared.

(
# equivalent of saved searches
cat <<EOF
"inbox" "notmuch://?query=tag:inbox"
"inbox-unread" "notmuch://?query=tag:unread and tag:inbox"
"unread" "notmuch://?query=tag:unread"
"todo" "notmuch://?query=tag:todo"
"sent" "notmuch://?query=tag:sent and date:30d.."
"draft" "notmuch://?query=tag:draft"
EOF

# all tags with unread messages, formatted, as above, as '"$TAG"
# "notmuch://?query=tag:$TAG"'
notmuch search --output=tags 'tag:unread' | \
    sed 's,\(.*\),  "\1" "notmuch://?query=tag:\1 and tag:unread" ,'
# mutt wants everything on one line for some reason, not sure why
) | tr '\n' ' '
