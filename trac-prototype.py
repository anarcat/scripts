#!/usr/bin/python3

import os
import xmlrpc.client


class TracClient:
    def __init__(self, url, username, password):
        self._client = xmlrpc.client.ServerProxy(url.format(**{
            "username": username,
            "password": password,
        }))

    def list_methods(self):
        for method in self._client.system.listMethods():
            print(method)
            print(self._client.system.methodHelp(method))
            print("")


if __name__ == '__main__':
    # TRAC_URL should be like: https://{username}:{password}@trac.torproject.org/projects/tor/login/rpc
    TracClient(os.environ.get('TRAC_URL'),
               os.environ.get('TRAC_USER'),
               os.environ.get('TRAC_PASSWORD')).list_methods()
