#!/usr/bin/python3

# prior art: https://github.com/YodaEmbedding/frece
#
# requires a special database and tool, might be useful for very large
# lists, but considered premature optimization otherwise

import argparse
from collections import defaultdict
import logging
import os
from pathlib import Path
import shlex
import subprocess
import sys
from typing import TextIO, Iterator


class LoggingAction(argparse.Action):
    """change log level on the fly

    The logging system should be initialized befure this, using
    `basicConfig`.
    """

    def __init__(self, *args, **kwargs):  # type: ignore[no-untyped-def]
        """setup the action parameters

        This enforces a selection of logging levels. It also checks if
        const is provided, in which case we assume it's an argument
        like `--verbose` or `--debug` without an argument.
        """
        kwargs["choices"] = logging._nameToLevel.keys()
        if "const" in kwargs:
            kwargs["nargs"] = 0
        super().__init__(*args, **kwargs)

    def __call__(self, parser, ns, values, option):  # type: ignore[no-untyped-def]
        """if const was specified it means argument-less parameters"""
        if self.const:
            logging.getLogger("").setLevel(self.const)
        else:
            logging.getLogger("").setLevel(values)
        # cargo-culted from _StoreConstAction
        setattr(ns, self.dest, self.const or values)


SAMPLE_KNOWN_HOSTS = """
abel.debian.org,abel,217.140.106.111 ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIDTdOahn5LSHotL876kVAxOjMn9UgeaoKCV6t/e/yDZF root@abel
amdahl.debian.org,amdahl,217.196.149.236,2a02:16a8:dc41:100::236 ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJT+xfxD2NxD0gIBb16YummLOS+Q9+djcBAm3udCYo+K root@amdahl
|1|CHTga7R0D1eT7ISoCCtOaxVclpk=|AehPyQbeOQN6Qp+S6S0uzGfnMG4= ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBKWcVrjZvNotQToD41hKhmH6uErFJ78y3znk1fLyQuT
""".split(
    "\n"
)  # noqa: E501


def parse_known_hosts(stream: TextIO) -> Iterator[str]:
    """parse the given known_hosts stream for host names

    >>> list(parse_known_hosts(SAMPLE_KNOWN_HOSTS))
    ['abel.debian.org', 'abel', '217.140.106.111', 'amdahl.debian.org', 'amdahl', '217.196.149.236', '2a02:16a8:dc41:100::236']
    """  # noqa: E501
    for line in stream:
        line = line.strip()
        logging.debug("parsing line: %r", line)
        if not line or line.startswith("#") or line.startswith("|1|"):
            continue
        hosts_blob, _ = line.split(" ", maxsplit=1)
        for host in hosts_blob.split(","):
            yield host


SAMPLE_SSH_CONFIG = """
Host foo foo.bar !foo
    Hostname 10.0.0.1
    HostKeyAlias blah.example.com

Host *.example.com
    Hostname 10.0.0.2
    UserKnownHostsFile ~/.ssh/known_hosts ~/.ssh/known_hosts.debian.org
""".split(
    "\n"
)


def parse_ssh_config_for_hosts(stream: TextIO) -> Iterator[str]:
    """parse the given ssh_config file stream for hostnames

    >>> list(parse_ssh_config_for_hosts(SAMPLE_SSH_CONFIG))
    ['foo', 'foo.bar', '10.0.0.1', 'blah.example.com', '10.0.0.2']
    """
    for line in stream:
        line = line.lower().strip()
        if (
            line.startswith("host ")
            or line.startswith("hostname ")
            or line.startswith("hostkeyalias ")
        ):
            hosts = line.split(" ")[1:]
            for host in hosts:
                if host.startswith("*") or host.startswith("!"):
                    continue
                yield host


def parse_ssh_config_for_known_hosts(stream: TextIO) -> Iterator[str]:
    """parse the given ssh_config file stream for known_hosts file

    >>> list(parse_ssh_config_for_known_hosts(SAMPLE_SSH_CONFIG))
    ['~/.ssh/known_hosts', '~/.ssh/known_hosts.debian.org']
    """
    for line in stream:
        line = line.lower().strip()
        if line.startswith("userknownhostsfile "):
            hostpaths = line.split(" ")[1:]
            for path in hostpaths:
                yield path


def default_hosts_files():
    """locations for default SSH known_hosts files"""
    return [
        "/etc/ssh/ssh_known_hosts",
        "~/.ssh/known_hosts",
    ]


# NOTE: this code is mostly deprecated, and has been replaced by
# fuzzel's --cache option in default operation. It's kept for
# historical reference but is also used when dmenu or a non-fuzzel
# launcher is used.
class History:
    """a "history" file representation

    A history file is more like a cache file but with usage frequency
    information. The file format is, somewhat (EBNF?) formally:

    <cache> ::= <cache> | <line>
    <line> ::= <integers> <SPACE> <string>

    I won't spell it out, but <integers> is any number of integers (at
    least one), and <string> is whatever comes after a single space.

    The class can read entries from a given `path` (provided in the
    constructor, defaulting to History.DEFAULT_PATH) with the
    `parse()` function.

    The `record()` function will add the given string to the internal
    cache or increment it if absent. It will call `parse()` if not
    already done but will *not* `write()` the resulting data back to
    the file.

    The `values()` method will return a list of strings sorted by
    occurence (but without the occurences).

    The actual occurences are stored in the `occurence` variable which
    is a str -> int dictionnary.
    """

    DEFAULT_PATH = b"~/.cache/dmenu-ssh"

    def __init__(self, path: bytes = DEFAULT_PATH):
        self.path = path.decode("utf-8")
        self.occurences = self.parse()

    def parse(self) -> dict[str, int]:
        """parse (or re-parse) the cache"""
        occurences: dict[str, int] = defaultdict(int)
        try:
            fp = Path(self.path).expanduser().open(encoding="utf-8")
        except OSError as e:
            logging.warning(str(e))
            return occurences
        for line in fp:
            count, data = line.strip().split(" ", maxsplit=1)
            occurences[data] = int(count)
        return occurences

    def record(self, data: str):
        """record an entry, does not write the cache"""
        if not self.occurences:
            self.occurences = self.parse()
        self.occurences[data] += 1

    def write(self):
        """write the cache file back"""
        path = Path(self.path).expanduser()
        logging.info("writing history to %s", path)
        with path.open("w", encoding="utf-8") as f:
            for key, value in sorted(
                self.occurences.items(), key=lambda item: item[1], reverse=True
            ):
                f.write("%d %s\n" % (value, key))

    def values(self) -> list[str]:
        """return values, but ordered by occurence"""
        values = []
        for k, v in sorted(
            self.occurences.items(), key=lambda item: item[1], reverse=True
        ):
            values.append(k)
        return values


# vendored in pass-domains
def sort_domains(domains: list[str]) -> list[str]:
    """this sorts a list of string by domain name

    That is, is the *second* label (technically, the third) in the
    parsed fully qualified domain name (FQDN).

    In other words, a lexicographically sorted list like this:

    example.com
    bar.example.net

    would instead be sorted like that:

    bar.example.net
    example.com

    IP address looking things are split out in a separate list and
    tacked at the end as well, deemed less interesting. They are also
    sorted differently.

    >>> sort_domains(["example.com", "bar.example.net"])
    ['example.com', 'bar.example.net']
    >>> sort_domains(["example.com", "foo.example.net"])
    ['example.com', 'foo.example.net']
    >>> sort_domains(["10.0.0.1", "example.com", "foo.example.net"])
    ['example.com', 'foo.example.net', '10.0.0.1']
    >>> sort_domains(["db", "foo", "10.0.0.1", "example.com"])
    ['example.com', 'db', 'foo', '10.0.0.1']
    >>> sort_domains(["bar.example.com", "bar", "foo", "foo.example.com"])
    ['bar.example.com', 'foo.example.com', 'bar', 'foo']
    """
    fqdns = []
    hosts = []
    ips = []
    # set() removes duplicates
    #
    # the rest of this loop creates a list of reversed parts of a
    # domain, so that "example.com" is stored as (["com", "example"],
    # "example.com"), which makes it possible to sort the domains
    # label, right to left.
    for domain in domains:
        for c in domain:
            if c not in "0123456789abcdef:.":  # not an IP address
                break
        else:
            # has only hex, colon and dot, so looks like an IP
            # make sure it has at least one dot (IPv4) or colon (IPv6)
            if "." in domain or ":" in domain:
                # count IPs separately as they don't sort the same way
                ips.append(domain)
                continue
        # not a FQDN, no need for special sort, and split out
        if "." not in domain:
            hosts.append(domain)
            continue
        parts = list(reversed(domain.split(".")))[1:] or [domain]
        fqdns.append((parts, domain))
    # and this is what does the final sort
    sorted_fqdns: list[str] = [domain for _, domain in sorted(fqdns)]
    # add hosts after FQDNs and IP addresses at the very end
    return sorted_fqdns + sorted(hosts) + sorted(ips)


def gather_hosts() -> list[str]:
    """search for host names in all possible locations

    This will:

    1. parse the .ssh/config for possible host names
    2. parse the default_hosts_files()
    3. parse the .ssh/config for known_hosts files
    4. parse those known_hosts file for hostnames
    5. sort and deduplicate the result
    """
    config = Path("~/.ssh/config").expanduser()

    try:
        hosts = list(parse_ssh_config_for_hosts(config.open()))
    except OSError as e:
        logging.warning("could not load SSH config: %s", e)
        hosts = []
    config_count = len(hosts)
    logging.info("found %d hosts in config file", config_count)

    for known_host_file in default_hosts_files():
        try:
            hosts += list(parse_known_hosts(Path(known_host_file).expanduser().open()))
        except OSError as e:
            logging.info("could not load known hosts file: %s", e)
    default_count = len(hosts) - config_count
    logging.info("found %d hosts in default hosts files", default_count)

    try:
        known_host_files = list(parse_ssh_config_for_known_hosts(config.open()))
    except OSError as e:
        logging.info("could not load SSH config: %s", e)
        known_host_files = []
    for known_host_file in known_host_files:
        try:
            hosts += list(parse_known_hosts(Path(known_host_file).expanduser().open()))
        except OSError as e:
            logging.warning("could not load SSH known hosts file: %s", e)
    special_count = len(hosts) - config_count - default_count
    logging.info("found %d hosts in other hosts files", special_count)

    # uniquify and sort the result
    return sort_domains(list(set(hosts)))


def main():
    """parse commandline, look for completions, start dmenu and act on the result"""
    logging.basicConfig(level="WARNING", format="%(message)s")
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-v",
        "--verbose",
        action=LoggingAction,
        const="INFO",
        help="enable verbose messages",
    )
    parser.add_argument(
        "-d",
        "--debug",
        action=LoggingAction,
        const="DEBUG",
        help="enable debugging messages",
    )
    parser.add_argument(
        "--dmenu",
        default=os.environ.get(
            "DMENU",
            "fuzzel --dmenu --cache=%s/.cache/fuzzel.d/dmenu-ssh"
            % os.environ.get("HOME"),
        ),
        help="which dmenu command to run, default: %(default)s",
    )
    parser.add_argument(
        "--ssh",
        default=os.environ.get("SSH", "ssh-wait-on-fail"),
        help="which ssh command to run, default: %(default)s",
    )
    parser.add_argument(
        "--terminal",
        default=os.environ.get("TERMINAL", "foot"),
        help="which terminal command to run, default: %(default)s",
    )
    args = parser.parse_args()
    args.dmenu = shlex.split(args.dmenu)
    args.ssh = shlex.split(args.ssh)
    args.terminal = shlex.split(args.terminal)

    gathered_hosts = gather_hosts()

    if "fuzzel" in args.dmenu and "--cache" in " ".join(args.dmenu):
        record_history = False
        logging.warning("using fuzzel cache")
        hosts = gathered_hosts
    else:
        record_history = True
        logging.warning("using built-in cache: %s", args.dmenu)
        # prepend cache
        history = History()
        hosts_from_history = history.values()
        logging.info("found %d hosts in history", len(hosts_from_history))

        # deduplicate gathered hosts from history
        for host in hosts_from_history:
            try:
                gathered_hosts.remove(host)
            except ValueError:
                pass
        hosts = hosts_from_history + gathered_hosts

    # start dmenu
    logging.debug("dumping %r into dmenu", hosts)
    try:
        dmenu = subprocess.Popen(
            args.dmenu, stdin=subprocess.PIPE, stdout=subprocess.PIPE
        )
        stdout, stderr = dmenu.communicate(input="\n".join(hosts).encode("utf-8"))
    except OSError as e:
        logging.error("failed to call %s: %s", args.dmenu, e)
        sys.exit(e.errno)
    final_host = stdout.decode("utf-8").strip()

    if not final_host:
        logging.info("no host selected, exiting")
        sys.exit(0)

    if record_history:
        # record in cache
        logging.info("recording %s in cache", final_host)
        history.record(final_host)
        try:
            history.write()
        except OSError as e:
            logging.warning("failed to write history file: %s", e)

    # launch SSH command in terminal
    command = args.terminal + ["-e"] + args.ssh + [final_host]
    logging.info("launching command %s", command)
    try:
        retcode = subprocess.call(command)
    except OSError as e:
        logging.error("failed to call %s: %s", args.dmenu, e)
        sys.exit(e.errno)
    sys.exit(retcode)


if __name__ == "__main__":
    main()
