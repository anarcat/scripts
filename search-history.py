#!/usr/bin/python3

"""dump the firefox search history form a places database"""

import argparse
from collections import Counter
from datetime import datetime
import logging
from pathlib import Path
from typing import Iterator
from urllib.parse import urlparse, parse_qs

import sqlite3


SEARCH_ENGINE_LIST = (
    # excludes av/proxy-image
    "https://www.startpage.com/search",
    # this excludes stuff like docs and /searchbyimage
    "https://www.google.com/search?",
    "https://duckduckgo.com/?",
    # alternate form
    "https://html.duckduckgo.com/?",
    "https://kagi.com/search",
    # this excludes stuff like search
    "https://kagi.com/?",
    "https://search.marginalia.nu/search",
    "https://www.qwant.com/?",
)

SEARCH_ENGINE_QUERY_KEYS = (
    "q",  # qwant, kagi, ddg
    "as_q",  # google
    "query",  # startpage
)


def main():
    logging.basicConfig(level="INFO")
    args = OurArgumentParser().parse_args()

    # deduplicate URLs
    found_urls = set()
    # deduplicate search terms
    found_search_terms = set()
    oldest = None
    count = Counter()
    for h in load_history(Path(args.places_database), patterns=args.patterns):
        url, last_visit_str = h
        logging.debug("res: %r, url: %s, last: %r", h, url, last_visit_str)

        if last_visit_str:
            # last_visit is in miliseconds
            last_visit = int(last_visit_str)/(10**6)

            if oldest is None or oldest > last_visit:
                oldest = last_visit
        count['entries'] += 1
        # shortcut: just dump URLs from search history if no search
        # engine extraction is requested
        if not args.search_engine_patterns and not args.extract_search_keys:
            if url in found_urls:
                count['duplicates'] += 1
                if args.deduplicate:
                    continue
            else:
                found_urls.add(url)
            count['urls'] += 1
            print(url)
            continue
        logging.debug("found URL %s", url)

        # search terms extraction
        search_terms = extract_terms_from_url(url, args.search_engine_query_keys)

        if search_terms is False:
            continue
        if search_terms in found_search_terms:
            count['duplicates'] += 1
            # deduplication
            if args.deduplicate:
                continue
        else:
            found_search_terms.add(search_terms)

        # finally, found a deduplicated search term, dump it
        print(search_terms)
        count['search terms'] += 1
    logging.info("counted %s", ", ".join("%s: %d" % x for x in count.items()))
    if oldest:
        logging.info("oldest URL is from %s", datetime.fromtimestamp(oldest).isoformat())


class OurArgumentParser(argparse.ArgumentParser):
    def __init__(self, *args, **kwargs):  # type: ignore[no-untyped-def]
        super().__init__(*args, description=__doc__, **kwargs)
        # it seems like Firefox doesn't really keep a search-specific history:
        # everything gets collected like normal URLs in the places.sqlite
        # database.
        #
        # TODO: parse ~/.mozilla/firefox/profiles.ini for the profile path
        # e.g.~/.mozilla/firefox/foo/ where places.sqlite is
        self.add_argument("--places-database", help="path to places.sqlite database", required=True)

        self.add_argument(
            "--deduplicate",
            action="store_true",
            help="deduplicate results, default: %(default)s",
        )
        self.add_argument(
            "--extract-search-keys",
            help="extract search terms if one of the given keys is found, default: %(default)s",
            action="store_true",
        )
        self.add_argument(
            "--search-engine-query-keys",
            help="query keys to search for in when --extract-search-keys is provided, default: %(default)s",
            nargs="+",
            default=SEARCH_ENGINE_QUERY_KEYS,
        )
        self.add_argument(
            "--search-engine-list",
            default=SEARCH_ENGINE_LIST,
            nargs="+",
            help="modify the built-in search engine list, default: %(default)s",
        )
        group = self.add_argument_group()
        group.add_argument(
            "--search-engine-patterns",
            "--search-terms",
            "--search-engine",
            "-s",
            action="store_true",
            help="use the built-in search engine pattern list, implies --extract-search-keys",
        )
        group.add_argument(
            "--patterns", nargs="+", default=[], help="search for given pattern(s)"
        )

    def parse_args(self, *args, **kwargs):  # type: ignore[no-untyped-def]
        args = super().parse_args(*args, **kwargs)
        if args.search_engine_patterns:
            args.patterns = args.search_engine_list
        return args


def extract_terms_from_url(url: str, query_keys: list[str]) -> str | None:
    """extract search terms from a query

    This turns example.com/?query=foo into foo, essentially.
    """
    # first, parse the URL and extract the "query" part (after the "?")
    query_string = urlparse(url).query
    # then parse the query string and extract the "query=" key
    parsed_query_string = parse_qs(query_string)
    for query_key in query_keys:
        s = parsed_query_string.get(query_key)
        if s is not None:
            search_terms = s[0]
            break
    else:
        logging.warning(
            "could not find a known query keys in URL %s (searched for %s)",
            url,
            query_keys,
        )
        return None
    return search_terms


def load_history(path: Path, patterns: list[str] = []) -> Iterator[tuple[str]]:
    """generate a list of URLs from the places database, matching the given pattern list"""
    db_path = str(path.absolute())
    logging.info("opening database file %s", db_path)
    con = sqlite3.connect("file:" + db_path + "?immutable=1", uri=True, timeout=0)
    patterns_query = " OR ".join([f"url LIKE '%{pattern}%'" for pattern in patterns])
    query = "SELECT url, last_visit_date FROM moz_places "
    if patterns:
        query += f" WHERE {patterns_query} "
    query += " ORDER BY last_visit_date;"
    logging.debug("query: %s", query)
    yield from con.execute(query)


if __name__ == "__main__":
    main()
