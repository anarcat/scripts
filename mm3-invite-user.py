#!/usr/bin/python3

'''Invite users to mailing lists instead of forcibly subscribing
them. Works around a limitation in Mailman 3, see this bug report for
details: <https://gitlab.com/mailman/mailman/issues/510>.
'''

import argparse
import getpass
import logging
import os

import urllib

import mailmanclient as mm


class LoggingAction(argparse.Action):
    """change log level on the fly

    The logging system should be initialized befure this, using
    `basicConfig`.
    """
    def __init__(self, *args, **kwargs):
        """setup the action parameters

        This enforces a selection of logging levels. It also checks if
        const is provided, in which case we assume it's an argument
        like `--verbose` or `--debug` without an argument.
        """
        kwargs['choices'] = logging._nameToLevel.keys()
        if 'const' in kwargs:
            kwargs['nargs'] = 0
        super().__init__(*args, **kwargs)

    def __call__(self, parser, ns, values, option):
        """if const was specified it means argument-less parameters"""
        if self.const:
            logging.getLogger('').setLevel(self.const)
        else:
            logging.getLogger('').setLevel(values)


logging.basicConfig(level='WARNING', format='%(levelname)s: %(message)s')

parser = argparse.ArgumentParser(epilog=__doc__,
                                 description='invite users to a mailing list')
parser.add_argument('--url', '-r', default='http://localhost:8001/3.1',
                    help='REST API endpoint (default: %(default)s)')
parser.add_argument('--user', '-u',
                    default=os.environ.get('MAILMAN_USER', 'restadmin'),
                    help='REST API user (default: %(default)s)')
parser.add_argument('--password', '-p',
                    default=os.environ.get('MAILMAN_PASSWORD'),
                    help='REST API password (default: %(default)s)')
parser.add_argument('--verbose', '-v', action=LoggingAction,
                    const='INFO', help='enable verbose messages')
parser.add_argument('--debug', '-d', action=LoggingAction,
                    const='DEBUG', help='enable debugging messages')
parser.add_argument('--no-notify', action='store_false', dest='notify',
                    help='do not send confirmation email')
parser.add_argument('list', help='mailing list address')
parser.add_argument('email', nargs='+', help='email addresses to invite')
args = parser.parse_args()

if not args.password:
    args.password = getpass.getpass('Password: ')

logging.debug('args: %s', args)
client = mm.Client(args.url, args.user, args.password)

logging.info('connected to %s with user %s running Mailman %s',
             args.url, args.user, client.system['mailman_version'])
logging.debug('client: %r', client)

mmlist = client.get_list(args.list)
logging.info('loaded list %s', args.list)
logging.debug('list: %r', mmlist)

for email in args.email:
    try:
        member = mmlist.subscribe(email, '',
                                  notify=args.notify,
                                  pre_verified=True,
                                  pre_confirmed=False,
                                  pre_approved=True)
    except urllib.error.HTTPError as httperror:
        logging.warning('failed to subscribe email %s: %s', email, httperror)
    else:
        logging.info('subscribed email %s', email)
        logging.debug('member: %r', member)

# template sent is list:user:action:subscribe, AKA "The message sent
# to subscribers when a subscription confirmation is required."

# if we get "Subscription already pending" we can do this:
#
# In [7]: for request in mmlist.requests:
#    ...:     mmlist.moderate_request(request['token'], 'discard')
