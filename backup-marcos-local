#!/bin/sh

# this script will run borg backups with a predefined exclusion list
# and some options
#
# it will also check to see if the external drive is mounted
# beforehand, to avoid filling up /media with a new backup, filling up
# the / directory.
#
# this script should not exist. there should instead be a borg cron
# subcommand that reads a config file and "borg cron" would then do
# the right thing.
#
# that would need to be implemented in borg/archiver.py, with a hook
# in run() and a def do_cron() that would call do_create() and others.
#
# unfortunately, upstream has showed hostility towards the idea (see
# #315) so it remains as a half-assed shell script for now.
#
# see also:
#
# * https://github.com/borgbackup/borg/issues/315
# * https://github.com/borgbackup/borg/issues/271
# * https://github.com/borgbackup/borg/issues/326
#
# this was originally written to replace bup, or more precisely, my
# wrapper named `bup-cron`
#
# features missing from bup-cron:
#
# * filesystem snapshots
# * parity blocks
# * syslog - support still missing from borg, although a logging
#   config file could be written to send stuff to syslog there's
#   no --syslog flag
# * timing information, although --stats does some of that

set -e

# this is the UUID of the encrypted partition (typically /dev/sdc2) in the "BOOK" device (/dev/sdc)
DEVICEID=6b6645de-194e-4262-8b4a-2cfef087a3b8
# the device we expect to have the crypted filesystem, i.e. the device after cryptsetup luksOpen
CRYPT_DEVICE_ID=a9b49f67-cf6d-4e0b-990c-126272f3aefd

# no servicable parts below
mountpoint=$(findmnt -n -f -o TARGET /dev/disk/by-uuid/$CRYPT_DEVICE_ID || true)
BORG_REPO="$mountpoint/borg"
export BORG_REPO

# custom tag for manual backups
tag=${1:-auto}

# this is how you bootstrap the repo
# borg init $BORG_REPO # todo: -e keyfile?

if [ ! -d "$BORG_REPO" ]; then
  mail -s "backups failed" root <<EOF
$BORG_REPO not found, possibly that our device is not mounted. i tried:

    findmnt -n -f -o TARGET /dev/disk/by-uuid/$DEVICEID

... to find the mountpoint, and failed. try this to fix the situation:

    # equivalent of pmount /dev/sdc2
    cryptsetup luksOpen "/dev/disk/by-uuid/$DEVICEID" "luks-$DEVICEID" &&
    LABEL=\$(blkid -o value -s LABEL "/dev/disk/by-id/dm-name-luks-$DEVICEID") &&
    mkdir -p "/media/\$LABEL" &&
    mount "/dev/disk/by-id/dm-name-luks-$DEVICEID" "/media/\$LABEL"

nightly backups failed, you're own you're own buddy. email fired from
$0
EOF
  exit 1
fi

export BORG_RELOCATED_REPO_ACCESS_IS_OK=yes
export BORG_UNKNOWN_UNENCRYPTED_REPO_ACCESS_IS_OK=yes

BORG_OPTS='--exclude-caches --keep-exclude-tags --one-file-system'
BORG_OPTS="$BORG_OPTS --exclude-if-present .nobackup "
BORG_OPTS="$BORG_OPTS --verbose --stats"

borg create $BORG_OPTS \
     -e "*/[Cc]ache/*" \
     -e "*.vmdk" \
     -e '/home/*/.cache/' \
     -e '*/.Trash-*/' \
     -e '*/.bitcoin/blocks/' \
     -e '*/build-area/*' \
     -e "/var/cache/*" \
     -e "/tmp/*" \
     -e "/var/tmp/*" \
     -e /srv/chroot \
     -e "/var/log/*" \
     ::"{hostname}-$tag-{now}" / /boot /usr /var /home

borg create --verbose --stats \
    ::'{hostname}-logs-{now}' \
    /var/log/
borg prune --verbose --list -d 30 -w 52 -y 10 --prefix "{hostname}-auto-"
borg prune --verbose --list -d 7 --prefix "{hostname}-logs-"
