#!/usr/bin/python3

import logging
import html
import sys
from typing import Iterator

import bs4
import requests


def parse_feed(content: str):
    soup = bs4.BeautifulSoup(content, "html.parser")
    assert soup
    # find the entries
    for entry in soup.find_all("entry"):
        logging.debug("found entry: %s, %s", entry, entry.title)
        if entry.title and entry.title.string.startswith("Current Conditions"):
            break
    else:
        raise ValueError("no entry title starting with 'Current Conditions', aborting")

    logging.debug("found current conditions: %s", entry)
    for summary in entry.find_all("summary"):
        logging.debug("found summary HTML: %s", entry.summary.text)
        summary = html.unescape(entry.summary.text)
        logging.debug("found decoded summary: %s", summary)
        return summary
    else:
        raise ValueError("no summary found in current contidions, aborting")


EXAMPLE_SUMMARY = """<b>Observed at:</b> Montréal-Trudeau Int'l Airport 10:00 AM EST Thursday 18 January 2024<br/> <b>Condition:</b> Mostly Cloudy<br/> <b>Temperature:</b> -8.7°C<br/> <b>Pressure / Tendency:</b> 101.6 kPa rising<br/> <b>Visibility:</b> 24 km<br/> <b>Humidity:</b> 67 %<br/> <b>Wind Chill:</b> -15<br/> <b>Dewpoint:</b> -13.8°C<br/> <b>Wind:</b> WSW 15 km/h<br/> <b>Air Quality Health Index:</b> n/a<br/>"""  # noqa: E501


EXAMPLE_METRICS = {
    "Condition": "Mostly Cloudy",
    "Temperature": "-8.7°C",
    "Pressure / Tendency": "101.6 kPa rising",
    "Visibility": "24 km",
    "Humidity": "67 %",
    "Wind Chill": "-15",
    "Dewpoint": "-13.8°C",
    "Wind": "WSW 15 km/h",
    "Air Quality Health Index": "n/a",
}

EXAMPLE_METRICS = {
    "ec_dew_point_celsius": " -13.8",
    "ec_humidity_percent": "67",
    "ec_pressure_kpa": "101.6",
    "ec_temperature_celsius": " -8.7",
    "ec_visibility_km": "24",
    "ec_wind_chill_celsius": " -15",
    "ec_wind_kph": "15",
}


METRICS_HELP = {
    "ec_temperature_celsius": "temperature in celsius",
    "ec_wind_kph": "wind speed in kilometer per hour",
    "ec_wind_gusts_kph": "wind gusts in kilometer per hour",
    "ec_wind_chill_celsius": "temperature with wind chill factor, in celsius",
    "ec_humidity_percent": "relative humidity",
    "ec_dew_point_celsius": "dew point",
    "ec_pressure_kpa": "pressure, in kilopascals",
    "ec_visibility_km": "visibility",
}


NAME_METRIC_MAP = {
    "Condition": "",
    "Temperature": "ec_temperature_celsius",
    "Pressure / Tendency": "ec_pressure_kpa",
    "Visibility": "ec_visibility_km",
    "Humidity": "ec_humidity_percent",
    "Wind Chill": "ec_wind_chill_celsius",
    "Dewpoint": "ec_dew_point_celsius",
    "Wind": "ec_wind_kph",
    "Air Quality Health Index": "ec_aqi",
}


def parse_summary(text: str) -> Iterator[tuple[str, str]]:
    for line in text.split("<br/>"):
        if not line:
            continue
        line = line.strip()
        if not line:
            continue
        logging.debug("line: %s", line)
        name, value = line.split(":</b>")
        name = name.strip().removeprefix("<b>").strip()
        metric = NAME_METRIC_MAP.get(name)
        if metric in (
            "ec_temperature_celsius",
            "ec_dew_point_celsius",
            "ec_wind_chill_celsius",
        ):
            yield metric, value.removesuffix("°C")
        elif metric in ("ec_pressure_kpa", "ec_visibility_km", "ec_humidity_percent"):
            value, _ = value.split(maxsplit=1)
            yield metric, value
        elif metric == "ec_wind_kph":
            # TODO: gusts
            _, value, _ = value.split(maxsplit=2)
            yield metric, value
        elif not metric:
            logging.debug("no matching metric name for %s", name)


def test_parse_summary():
    assert dict(parse_summary(EXAMPLE_SUMMARY)) == EXAMPLE_METRICS


def main():
    logging.basicConfig(level="INFO")
    station = 147
    station_name = "Montréal, QC"

    url = f"https://meteo.gc.ca/rss/city/qc-{station}_e.xml"

    logging.debug("fetching %s", url)
    try:
        resp = requests.get(url)
        resp.raise_for_status()
    except requests.RequestException as e:
        logging.error("failed to load URL %s: %s", url, e)
        sys.exit(1)

    logging.debug("fetched %d bytes from %s", len(resp.content), url)
    try:
        summary = parse_feed(resp.content)
    except (
        AssertionError,
        ValueError,
        AttributeError,
        bs4.ParserRejectedMarkup,
        bs4.StopParsing,
    ) as e:
        logging.error("failed to parse URL %s: %s", url, e)
        sys.exit(1)
    for metric, value in parse_summary(summary):
        print("# HELP %s %s" % (metric, METRICS_HELP[metric]))
        print("# TYPE %s gauge" % metric)
        print(metric + '{station="%s"}' % station_name, value)
        print()


if __name__ == "__main__":
    main()
