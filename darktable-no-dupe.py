#! /usr/bin/python

import os
import sqlite3
import argparse

parser = argparse.ArgumentParser(description='Group duplicate photos in Darktable.')
parser.add_argument('--dryrun', '-n', action='store_true')
parser.add_argument('--verbose', '-v', action='store_true')
parser.add_argument('--debug', '-d', action='store_true')
parser.add_argument('--limit', '-l', type=int, default=10)

args = parser.parse_args()
if args.dryrun:
    print "not doing any changes"

def maybe_print(msg):
    if args.verbose:
        print msg,

print 'loading darktable database...'
darktable = sqlite3.connect(os.getenv('HOME', '.') + '/.config/darktable/library.db')
darktable.row_factory = sqlite3.Row

# works only for CR2 files for now
dupes = darktable.execute('''
SELECT r1.folder || "/" || i.filename AS path1, i.id, i.group_id AS group1, r2.folder || "/" || i2.filename AS path2, i2.id AS id2, i2.group_id
    FROM images i
    INNER JOIN film_rolls r1 ON r1.id = i.film_id
    JOIN images i2
    INNER JOIN film_rolls r2 ON r2.id = i2.film_id
        WHERE path1 NOT LIKE '%.jpg'
              AND rtrim(path1, ".CR2") || "_CR2.jpg" LIKE path2
              AND i.group_id <> i2.group_id
    LIMIT ?
''', (args.limit, ))

found = 0
for dupe in dupes:
    args.debug and maybe_print("\t".join([str(d) for d in dupe]) + "\n")
    args.dryrun and maybe_print('would be')
    query = 'UPDATE images SET group_id = ? WHERE id = ?'
    maybe_print('executing ' + query.replace('?', '%d') % (dupe['group1'], dupe['id2']) + "\n")
    if not args.dryrun:
        assert darktable.execute(query, (dupe['group1'], dupe['id2'])).rowcount == 1
        found += 1
if not args.dryrun:
    darktable.commit()
print 'grouped %d images' % found
