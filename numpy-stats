#!/usr/bin/python3

"""generate basic stats from a text file, using numpy. output is
JSON-formatted but should be readable by normal humans."""


import argparse
import json


import numpy as np


def main():
    parser = argparse.ArgumentParser(epilog=__doc__)
    parser.add_argument("input", help="input file, parsable by numpy")
    parser.add_argument(
        "--column",
        "-c",
        type=str,
        default=None,
        help="operate only on given column name",
    )
    args = parser.parse_args()
    data = np.genfromtxt(
        args.input, names=args.column is not None or None, delimiter=",", dtype=None
    )
    if args.column is not None:
        data = data[args.column]
    info = {
        "size": data.size,
        "min": np.nanmin(data),
        "max": np.nanmax(data),
        "median": np.nanmedian(data),
        "mean": np.nanmean(data),
        "std": np.nanstd(data),
    }
    print(json.dumps(info, indent=2, sort_keys=True))


if __name__ == "__main__":
    main()
