#!/usr/bin/python3

"""count words or line changes per author in a git repository"""

import argparse
from collections import Counter
import shlex
import logging
import re
import subprocess

logging.basicConfig(level="INFO", format="%(message)s")
parser = argparse.ArgumentParser(description=__doc__)
parser.add_argument(
    "--since", default="1 year ago", help="how far back to look, default: %(default)s"
)
parser.add_argument(
    "--kind",
    choices=["lines", "words"],
    default="words",
    help="what kind of diff to parse, default: %(default)s",
)
args = parser.parse_args()

command = ["git", "log", "--since=" + args.since, "--format=%H %al"]
log = subprocess.Popen(
    command,
    stdout=subprocess.PIPE,
    encoding="utf-8",
)
logging.info(
    "parsing commits for %s changes from command: %s", args.kind, shlex.join(command)
)
assert log.stdout
authors_add: dict[str, int] = Counter()
authors_del: dict[str, int] = Counter()
for line in log.stdout:
    commit_hash, author = line.strip().split(maxsplit=1)
    logging.debug("parsing commit %s from %s", commit_hash, author)

    if args.kind == "lines":
        diff_arg = "--shortstat"
    elif args.kind == "words":
        diff_arg = "--word-diff=porcelain"
    command = [
        "git",
        "diff",
        diff_arg,
        commit_hash + "^.." + commit_hash,
    ]
    diff = subprocess.Popen(
        command,
        stdout=subprocess.PIPE,
        encoding="utf-8",
    )
    logging.debug("running command %s", shlex.join(command))
    assert diff.stdout
    for diffline in diff.stdout:
        # --shortstat looks like:
        #  1 file changed, 57 insertions(+), 41 deletions(-)
        if args.kind == "lines":
            m = re.search(r"(\d+) insertions?", diffline)
            if m:
                authors_add[author] += int(m.group(1))
            m = re.search(r"(\d+) deletions?", diffline)
            if m:
                authors_del[author] += int(m.group(1))
            continue

        # a --word-diff=porcelain looks like this:
        #
        # diff --git a/howto/raid.md b/howto/raid.md
        # index 95e75962..db191437 100644
        # --- a/howto/raid.md
        # +++ b/howto/raid.md
        # @@ -6,7 +6,7 @@
        #
        #  If a drive fails in a server, the procedure is essentially to open a
        #  ticket, wait for the drive change, partition and re-add it to the RAID
        # -array. The following procdure assumes that `sda` failed and `sdb` is
        # +array. The following procedure assumes that `sda` failed and `sdb` is
        #  good in a RAID-1 array, but can vary with other RAID configurations or
        #  drive models.
        #
        # looks like:
        #
        #
        # diff --git a/howto/raid.md b/howto/raid.md
        # index 95e75962..db191437 100644
        # --- a/howto/raid.md
        # +++ b/howto/raid.md
        # @@ -6,7 +6,7 @@
        #
        # ~
        #  If a drive fails in a server, the procedure is essentially to open a
        # ~
        #  ticket, wait for the drive change, partition and re-add it to the RAID
        # ~
        #  array. The following
        # -procdure
        # +procedure
        #   assumes that `sda` failed and `sdb` is
        # ~
        #  good in a RAID-1 array, but can vary with other RAID configurations or
        # ~
        #  drive models.
        # ~
        #
        # ~
        #
        # we basically ignore everything but the -/+ lines here.

        # skip hunk headers that might confuse later parser
        if diffline.startswith(("+++", "---")):
            logging.debug("skipping header line %s", diffline)
            continue
        # skip short lines
        if len(diffline) <= 1:
            logging.debug("skipping short line %s", diffline)
            continue
        # only keep +- lines
        if not diffline.startswith(("+", "-")):
            continue
        logging.debug("parsing word-diff=porcelain line %s", diffline)
        # count words after +- marker
        count = len(diffline[1:].split())
        if diffline[0] == "-":
            authors_del[author] += count
        elif diffline[0] == "+":
            authors_add[author] += count
    assert diff.wait() == 0
assert log.wait() == 0

for author in sorted(set(authors_del.keys()) | set(authors_add.keys())):
    addition = authors_add.get(author, 0)
    deletion = authors_del.get(author, 0)
    total = addition - deletion
    print(author, addition, "-", deletion, "= %d" % total)
