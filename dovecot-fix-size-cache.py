#!/usr/bin/python3
# coding: utf-8

"""fix Dovecot's S= cache sizes"""

# Copyright (C) 2016 Antoine Beaupré <anarcat@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import division, absolute_import
from __future__ import print_function, unicode_literals

import argparse
import logging
import os.path
import sys
import re


class LoggingAction(argparse.Action):
    """change log level on the fly

    The logging system should be initialized befure this, using
    `basicConfig`."""

    def __init__(self, *args, **kwargs):
        """setup the action parameters

        This enforces a selection of logging levels. It also checks if
        const is provided, in which case we assume it's an argument
        like `--verbose` or `--debug` without an argument.
        """
        kwargs["choices"] = logging._nameToLevel.keys()
        if "const" in kwargs:
            kwargs["nargs"] = 0
        super().__init__(*args, **kwargs)

    def __call__(self, parser, ns, values, option):
        """if const was specified it means argument-less parameters"""
        if self.const:
            logging.getLogger("").setLevel(self.const)
        else:
            logging.getLogger("").setLevel(values)
        # cargo-culted from _StoreConstAction
        setattr(ns, self.dest, self.const or values)


def parse_args(args=sys.argv[1:]):
    parser = argparse.ArgumentParser(description=__doc__)
    logging.basicConfig(level="WARNING", format="%(message)s")
    parser.add_argument(
        "-v",
        "--verbose",
        action=LoggingAction,
        const="INFO",
        help="enable verbose messages",
    )
    parser.add_argument(
        "-d",
        "--debug",
        action=LoggingAction,
        const="DEBUG",
        help="enable debugging messages",
    )
    parser.add_argument("--dryrun", "-n", action="store_true", help="do nothing")
    parser.add_argument("--maildir", help="maildir to inspect", default="Maildir/")
    return parser.parse_args(args=args)


def walk_handler(exc):
    logging.warning("scandir failed on %s: %s", exc.filename, exc)


def main():
    args = parse_args()
    for root, dirs, files in os.walk(args.maildir, onerror=walk_handler):
        for name in files + dirs:
            path = os.path.join(root, name)
            logging.debug("# checking %s", path)

            REGEX = re.compile(r"^(.*),S=(\d+)(.*$)")
            m = REGEX.match(name)
            if m:
                cached_size = int(m.group(2))
                s = os.stat(path)
                if s.st_size == 0:
                    logging.warning("empty message: %s", path)
                    continue
                if cached_size != s.st_size:
                    logging.warning(
                        "file size (%d) different than cached (%d): %s",
                        s.st_size,
                        cached_size,
                        path,
                    )
                    new_name = REGEX.sub(r"\1,S=%s\3" % s.st_size, name)
                    new_path = os.path.join(root, new_name)
                    if args.dryrun:
                        logging.warning("would rename to %s", new_path)
                    else:
                        logging.warning("renaming to %s", new_path)
                        os.rename(path, new_path)
                        path = new_path

            REGEX = re.compile(r"^(.*),W=(\d+)(.*$)")
            m = REGEX.match(name)
            if m:
                w_size = int(m.group(2))
                # XXX: possibly memory hungry
                with open(path, 'rb') as fp:
                    crlf_size = len(re.sub(b'\n', b'\r\n', fp.read()))
                if crlf_size != w_size:
                    logging.warning("wrong W size: %d, expected %s on %s", w_size, crlf_size, path)
                    new_name = REGEX.sub(r"\1,W=%d\3" % crlf_size, name)
                    new_path = os.path.join(root, new_name)
                    if args.dryrun:
                        logging.warning("would rename to %s", new_path)
                    else:
                        logging.warning("renaming to %s", new_path)
                        os.rename(path, new_path)
                    path = new_path


if __name__ == "__main__":
    main()
